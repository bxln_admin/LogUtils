package com.gitee.androidgoodies.liblogutils;
import android.view.View;
import android.content.Context;
import android.util.AttributeSet;
import android.content.res.TypedArray;
import android.util.Log;
import android.os.Handler;
import java.lang.ref.WeakReference;
import android.os.Message;
import android.widget.TextView;
import android.widget.LinearLayout;
import java.util.ArrayList;
import android.widget.ScrollView;

public class LogView extends LinearLayout {
    public static final String TAG = LogView.class.getSimpleName();
    ArrayList<String> mlistTAG;
    int mnLevel;
    int mnLastCount;
    ScrollView msvLog;
    TextView mtvMSG;
    
    RealTimeLoadLogThread mRealTimeLoadLogThread = null;
    Handler mHandler;
    
    /**
     * 在java代码里new的时候会用到
     * @param context
     */
    public LogView(Context context) {
        super(context);
    }

    /**
     * 在xml布局文件中使用时自动调用
     * @param context
     */
    public LogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.LogView);
        mlistTAG = new ArrayList<String>();
        String mszTagListString = ta.getString(R.styleable.LogView_tag);
        for(String sz : mszTagListString.split(",")) {
            mlistTAG.add(sz);
        }
        mnLevel = ta.getInteger(R.styleable.LogView_level, 0);
        mnLastCount = ta.getInteger(R.styleable.LogView_lastcount, 0);
        ta.recycle();
        mHandler = new LogViewHandler(this);
        mtvMSG = new TextView(context);
        msvLog = new ScrollView(context);
        msvLog.addView(mtvMSG);
        addView(msvLog);
        //mtvMSG.setText("Hello, World!");
    }

    /**
     * 不会自动调用，如果有默认style时，在第二个构造函数中调用
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    public LogView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    /**
     * 只有在API版本>21时才会用到
     * 不会自动调用，如果有默认style时，在第二个构造函数中调用
     * @param context
     * @param attrs
     * @param defStyleAttr
     * @param defStyleRes
     */
    public LogView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
    
    public void appenMSG(String sz) {
        mtvMSG.append(sz +"\n");
        scrollTextView();
    }
    
    public void startLog() {
        if (mRealTimeLoadLogThread == null) {
            mRealTimeLoadLogThread = new RealTimeLoadLogThread(mHandler, mlistTAG, mnLevel, mnLastCount);
            mRealTimeLoadLogThread.start();
        } else {
            Log.d(TAG, "Start log failed.");
        }
        
    }
    
    void scrollTextView() {
        final ScrollView sv = msvLog;
        sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.fullScroll(ScrollView.FOCUS_DOWN);
                }
            });
    }
}
