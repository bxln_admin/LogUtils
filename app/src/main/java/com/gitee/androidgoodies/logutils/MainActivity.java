package com.gitee.androidgoodies.logutils;
 
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.Toast;
import com.gitee.androidgoodies.liblogutils.LogViewFragment;
import android.app.FragmentTransaction;
import com.gitee.androidgoodies.liblogutils.LogView;
import android.util.Log;

public class MainActivity extends Activity { 

    LogViewFragment mLogViewFragment;
    
    LogView mLogView;
     
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLogView = findViewById(R.id.activitymainLogView1);
        
        mLogView.startLog();
    }
    
	public void onTestLogViewActivity(View v) {
        if(isInMultiWindowMode()) {
            Intent i = new Intent(MainActivity.this, com.gitee.androidgoodies.liblogutils.LogViewActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        } else {
            Toast.makeText(getApplication(), "Set current window to MultiWindowMode first.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onTestLogViewFragment(View v) {
       /* if(mLogViewFragment  == null) {
            mLogViewFragment = new LogViewFragment();
            FragmentTransaction tx = getFragmentManager().beginTransaction();
            tx.add(R.id.activitymainFrameLayout1, mLogViewFragment, LogViewFragment.TAG);
            tx.commit();
        }*/
        
    }
    
    public void onLog(View v) {
        Log.v("Test", "tv");
        Log.v("Test2", "tv");
        Log.v("Test3", "tv");
        Log.d("Test", "td");
        Log.d("Test2", "td");
        Log.d("Test3", "td");
        Log.i("Test", "ti");
        Log.i("Test2", "ti");
        Log.i("Test3", "ti");
    }
} 
